import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import charactersPageSlice from "./characters/characters.slice";
import {watchCharacterList} from "./characters/character.saga";
import { all } from 'redux-saga/effects';
import createSagaMiddleware from 'redux-saga';
import {TAnyGenerator} from '../core/core.type';
import {watchDictionaries} from "./dictionaries/dictionaries.saga";
import dictionariesSlice from "./dictionaries/dictionaries.slice";

function* rootSaga(): TAnyGenerator {
    yield all([watchCharacterList(), watchDictionaries()]);
}

const sagaMiddleware = createSagaMiddleware();

export const store = configureStore({
    reducer: {
        characters: charactersPageSlice.reducer,
        dictionary: dictionariesSlice.reducer,
    },
    middleware: [...getDefaultMiddleware({thunk: false}), sagaMiddleware],
});

sagaMiddleware.run(rootSaga);
