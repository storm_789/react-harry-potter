import {all, call, put, select, takeLatest} from 'redux-saga/effects';
import {getCharacterById, getCharactersList} from "../../services/characterService/character.service";
import {TAnyGenerator} from '../../core/core.type';
import {charactersPageActions} from "./characters.slice";
import {TAppState} from "../store.type";
import {IFilterParams} from "./characters.type";
import {MAX_CHARACTER_LIST_LENGTH} from "../../services/characterService/constants";


export const formFilters = (state: TAppState): IFilterParams => state.characters.filterFormParam;
export const selectedCardId = (state: TAppState): string => state.characters.selectedCardId;

function* setCharactersSaga(): TAnyGenerator {

    const {gender, race, side, input} = yield select(formFilters);

    const options = {
        gender,
        race,
        side,
        values: input,
        page: 0,
        size: MAX_CHARACTER_LIST_LENGTH,
    }

    const currentCharacterList = yield call(getCharactersList, options);
    yield put(charactersPageActions.setCurrentCharacterList(currentCharacterList));
}

function* setSelectedCard(): TAnyGenerator {
    const id = yield select(selectedCardId);
    const selectedCard = yield call(getCharacterById, id);

    yield put(charactersPageActions.setSelectedCard(selectedCard));
}

export function* watchCharacterList(): TAnyGenerator {
    yield all([
        takeLatest(charactersPageActions.fetchCurrentCharacterList, setCharactersSaga),
        takeLatest(charactersPageActions.fetchSelectedCard, setSelectedCard),
        takeLatest(charactersPageActions.setFormFilters, setCharactersSaga),
    ])
}
