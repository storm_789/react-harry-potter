import {ICharacter} from "../../components/registry/registry.type";

export interface IFilterParams {
    gender?: string[] | boolean[];
    side?: string[] | boolean[];
    race?: string[] | boolean[];
    input?: string | boolean[];
}

export interface ICharacterSlice {
    filterFormParam: IFilterParams;
    currentCharacterList: ICharacter[];
    selectedCard: ICharacter | null;
    selectedCardId: string;
    isNewCharacterModalOpen: boolean;
    isCharModalByIdOpen: boolean;
}
