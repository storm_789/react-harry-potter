import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {ICharacterSlice, IFilterParams} from "./characters.type";
import {ICharacter} from "../../components/registry/registry.type";

const initialState: ICharacterSlice  = {
    filterFormParam: {},
    currentCharacterList: [],
    selectedCard: null,
    selectedCardId: '',
    isNewCharacterModalOpen: false,
    isCharModalByIdOpen: false,
};

const charactersPageSlice = createSlice({
    name: 'charactersPage',
    initialState,
    reducers: {
        setFormFilters(state, action: PayloadAction<IFilterParams>) {
            state.filterFormParam = JSON.parse(JSON.stringify(action.payload));
        },
        fetchCurrentCharacterList() {},
        setCurrentCharacterList(state, action: PayloadAction<ICharacter[]>) {
            state.currentCharacterList = action.payload.reverse();
        },
        toggleNewModalOpen(state, action: PayloadAction<boolean>) {
            state.isNewCharacterModalOpen = action.payload;
        },
        toggleCharInfoModalOpen(state, action: PayloadAction<boolean>) {
            state.isCharModalByIdOpen = action.payload;
        },
        fetchSelectedCard(state, action: PayloadAction<string>) {
            state.selectedCardId = action.payload;
        },
        setSelectedCard(state, action: PayloadAction<ICharacter>) {
            state.selectedCard = action.payload;
            console.log(state.selectedCard)
        },
        clearSelectedCard(state) {
            state.selectedCard = initialState.selectedCard;
        }
    },
});

export default charactersPageSlice;
export const charactersPageActions = charactersPageSlice.actions;
