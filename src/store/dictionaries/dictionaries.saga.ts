import {all, call, put, takeLatest} from 'redux-saga/effects';
import {TAnyGenerator} from '../../core/core.type';
import {getGender, getRace, getSide} from "../../services/selectService/select.service";
import {dictionariesActions} from "./dictionaries.slice";

function* setGender(): TAnyGenerator {
    const genderOptions = yield call(getGender);
    yield put(dictionariesActions.setGenderOptions(genderOptions));
}

function* setSide(): TAnyGenerator {
    const sideOptions = yield call(getSide);
    yield put(dictionariesActions.setSideOptions(sideOptions));
}

function* setRace(): TAnyGenerator {
    const raceOptions = yield call(getRace);
    yield put(dictionariesActions.setRaceOptions(raceOptions));
}

export function* watchDictionaries(): TAnyGenerator {
    yield all([
        takeLatest(dictionariesActions.fetchGender, setGender),
        takeLatest(dictionariesActions.fetchRace, setRace),
        takeLatest(dictionariesActions.fetchSide, setSide),
    ])
}
