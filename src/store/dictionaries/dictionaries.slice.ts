import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {IDictionariesSlice, IOptions} from "./dictionaries.type";

const initialState: IDictionariesSlice  = {
    gender: [],
    side: [],
    race: [],
};

const dictionariesSlice = createSlice({
    name: 'dictionariesPage',
    initialState,
    reducers: {
        fetchGender() {},
        fetchRace() {},
        fetchSide() {},
        setGenderOptions(state, action: PayloadAction<IOptions[]>) {
            console.log(action.payload)
            state.gender = action.payload;
        },
        setSideOptions(state, action: PayloadAction<IOptions[]>) {
            console.log(action.payload)
            state.side = action.payload;
        },
        setRaceOptions(state, action: PayloadAction<IOptions[]>) {
            console.log(action.payload)
            state.race = action.payload;
        },
    },
});

export default dictionariesSlice;
export const dictionariesActions = dictionariesSlice.actions;
