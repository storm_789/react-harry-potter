export interface IDictionariesSlice {
    gender:IOptions[];
    side: IOptions[];
    race: IOptions[];
}

export interface IOptions {
    id: string;
    value: string;
}
