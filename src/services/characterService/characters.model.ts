import {ISelectModel} from "../selectService/select.model";

export interface ICharacterContentList  {
    id: string;
    name: string;
    imageURL: string;
    nameColor: string;
    backgroundColor: string;
    parametersColor: string;
    gender: string;
    race: string;
    side: string;
}

export interface ICharactersListModel {
    content: ICharacterContentList[];
    first: boolean,
    last: boolean,
    number: number;
    numberOfElements: number;
    size: number;
    totalElements: number;
    totalPages: number;
}

export interface ICharacterByIdModel {
    id: string,
    name: string;
    description: string;
    imageURL: string;
    nameColor: string;
    backgroundColor: string;
    parametersColor: string;
    tag1: string;
    tag2: string;
    tag3: string;
    gender: ISelectModel;
    race: ISelectModel;
    side: ISelectModel;
}
