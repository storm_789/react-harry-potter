import {ICharacterByIdModel, ICharactersListModel} from "./characters.model";
import {characterByIdFactory, characterFactory} from "../../core/factory/character.factory";
import {ICharacter} from '../../components/registry/registry.type';
import qs from 'qs';

export interface IOptions {
    gender?: string;
    race?: string;
    side?: string;
    values?: string;
    page: number;
    size: number;
    sort?: string;
}

export const getCharactersList = async (options: IOptions): Promise<ICharacter[]> => {
    const response = await fetch(`http://localhost:5000/api/HARRY_POTTER/character?${qs.stringify(options)}`);
    const data: ICharactersListModel = await response.json();

    return characterFactory(data.content);
}


export const getCharacterById = async (id: string): Promise<ICharacter> => {
    const response = await fetch(`http://localhost:5000/api/HARRY_POTTER/character/${id}`);
    const data: ICharacterByIdModel = await response.json();

    return characterByIdFactory(data);
}

export const addNewCharacter = async (newChar: any): Promise<void> => {
    await fetch(`http://localhost:5000/api/HARRY_POTTER/character`, {
        method: 'POST',
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify(newChar)
    });
}
