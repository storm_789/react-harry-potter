import {IOptions} from "../../store/dictionaries/dictionaries.type";
import {ISelectModel} from "./select.model";
import {dictionariesFactory} from "../../core/factory/dictionaries.factory";

export const getGender = async (): Promise<IOptions[]> => {
    const response = await fetch(`http://localhost:5000/api/HARRY_POTTER/gender`);
    const data: ISelectModel[] = await response.json();

    return dictionariesFactory(data);
};

export const getSide = async (): Promise<IOptions[]> => {
    const response = await fetch(`http://localhost:5000/api/HARRY_POTTER/side`);
    const data: ISelectModel[] = await response.json();

    return dictionariesFactory(data);
};

export const getRace = async (): Promise<IOptions[]> => {
    const response = await fetch(`http://localhost:5000/api/HARRY_POTTER/race`);
    const data: ISelectModel[] = await response.json();

    return dictionariesFactory(data);
};
