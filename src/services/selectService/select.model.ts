export interface ISelectModel {
    id: string;
    value: string;
}
