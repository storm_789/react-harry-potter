export const gender = [
    { id: 1, value: 'женщина' },
    { id: 2, value: 'мужчина' },
    { id: 3, value: 'неопределен' },
];

export const race = [
    { id: 1, value: 'человек' },
    { id: 2, value: 'получеловек' },
];

export const side = [
    { id: 1, value: 'добро' },
    { id: 2, value: 'зло' },
    { id: 3, value: 'хаос' },
];
