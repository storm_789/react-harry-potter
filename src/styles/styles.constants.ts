export const LARGE_BUTTON_COLOR = '#B09A81';
export const LARGE_BUTTON_HOVER_COLOR = '#DAAF7C';
export const LARGE_BUTTON_PRESSED_COLOR = '#B08F67';

export const BUTTON_COLOR = '#1F4F3B';
export const BUTTON_HOVER_COLOR = '#2D653A';
export const BUTTON_PRESSED_COLOR = '#296036';

export const BUTTON_SMALL_COLOR = '#1F4F3B';
export const BUTTON_SMALL_HOVER_COLOR = '#2D653A';
export const BUTTON_SMALL_PRESSED_COLOR = '#B09A81';
export const BUTTON_SMALL_CHANGE_COLOR = '#B08F67';

export const FILTER_FIELDS_BACKGROUND_COLOR = '#B09A81';

export const CHECKBOX_COLOR = '#1F4F3B';
