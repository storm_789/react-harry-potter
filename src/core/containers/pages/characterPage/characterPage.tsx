import {FC, useEffect, useState} from "react";
import {MAX_CHARACTER_LIST_LENGTH} from "../../../../services/characterService/constants";
import background from '../../../../styles/img/character_bg.png';
import {CharacterPageContainer} from "./characterPageStyles/characterPage.styles";
import Registry from "../../../../components/registry/registry";
import SearchForm from "./searchCharacterForm/searchCharacterForm";
import {useAppDispatch, useAppSelector} from "../../../../store/store.hooks";
import {charactersPageActions} from "../../../../store/characters/characters.slice";
import NewCharModal from "./characterPageModals/newCharModal/newCharModal";
import {useHistory, useParams} from "react-router-dom";
import CharInfoByIdModal from "./characterPageModals/charInfoByIdModal/charInfoByIdModal";

const CharacterPage: FC = () => {
    const { id } = useParams<{id:string}>();
    const history = useHistory();
    const dispatch = useAppDispatch();
    const currentList = useAppSelector(state => state.characters.currentCharacterList);
    const isNewCharModalOpen = useAppSelector(state => state.characters.isNewCharacterModalOpen);
    const isCharModalByIdOpen = useAppSelector(state => state.characters.isCharModalByIdOpen);

    useEffect(() => {
        dispatch(charactersPageActions.fetchCurrentCharacterList());
        return (() => {
            localStorage.removeItem('cardId')
        });
    }, []);

    useEffect(() => {
        if (id === 'new') {
            dispatch(charactersPageActions.toggleNewModalOpen(true));
            history.replace(`/characters/new`)
        } else if (id) {
            dispatch(charactersPageActions.toggleCharInfoModalOpen(true));
            history.replace(`/characters/${id}`)
            localStorage.setItem('cardId', id);
        }
    }, []);

    useEffect(() => {
        isNewCharModalOpen ? history.replace(`/characters/new`) : history.replace(`/characters`);
    }, [isNewCharModalOpen])

    useEffect(() => {
        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
        !isCharModalByIdOpen ? history.replace(`/characters`) : false;
    }, [isCharModalByIdOpen])

    return (
        <CharacterPageContainer background={background}>
            <div style={{display: 'flex', flexDirection: 'column', height: '100%', paddingTop: '25px'}}>
                <SearchForm />
                <Registry
                    registryProps={
                        {
                            registryList: currentList,
                            maxListLength: MAX_CHARACTER_LIST_LENGTH
                        }
                    }
                />
                { isNewCharModalOpen ? <NewCharModal /> : null }
                { isCharModalByIdOpen ? <CharInfoByIdModal /> : null }
            </div>
        </CharacterPageContainer>
    );
};

export default CharacterPage;
