import styled from "styled-components";
import {FILTER_FIELDS_BACKGROUND_COLOR, LARGE_BUTTON_HOVER_COLOR} from "../../../../../styles/styles.constants";

export const SelectContainer = styled.div`
  display: flex;
  gap: 35px;
  margin-top: 15px;
`;

export const NewCharButton = styled.button`
  width: 113px;
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 12px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.08);
  border-radius: 4px;
  font-size: inherit;
  font-family: inherit;
  cursor: pointer;
  border: none;
  background-color: ${FILTER_FIELDS_BACKGROUND_COLOR};
  
  &:hover {
    background-color: ${LARGE_BUTTON_HOVER_COLOR};
  }
`;
