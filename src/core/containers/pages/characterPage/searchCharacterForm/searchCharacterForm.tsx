import {FC, ReactElement, useEffect} from "react";
import {TRANSLATE_CHARACTERS_LIST} from "../../../../../services/characterService/constants";
import {FILTER_FIELDS_BACKGROUND_COLOR} from "../../../../../styles/styles.constants";
import arrow from "../../../../../styles/img/openSelect.png";
import Input from "../../../../../components/formInput/input";
import Select from "../../../../../components/select/select";
import {NewCharButton, SelectContainer} from "./searchCharacterForm.style";
import {useHistory} from "react-router-dom";
import {useAppDispatch, useAppSelector} from "../../../../../store/store.hooks";
import {charactersPageActions} from "../../../../../store/characters/characters.slice";
import {Field, Form, FormSpy} from "react-final-form";
import {dictionariesActions} from "../../../../../store/dictionaries/dictionaries.slice";

const SearchForm: FC = () => {
    const dispatch = useAppDispatch();
    const formParams = useAppSelector(state => state.characters.filterFormParam);
    const gender = useAppSelector(state => state.dictionary.gender);
    const race = useAppSelector(state => state.dictionary.race);
    const side = useAppSelector(state => state.dictionary.side);
    const history = useHistory();
    const url = new URL(window.location.href);

    useEffect(() => {
        dispatch(dictionariesActions.fetchGender());
        dispatch(dictionariesActions.fetchRace());
        dispatch(dictionariesActions.fetchSide());
    }, []);

    useEffect(() => {
        if (!Object.keys(formParams).length) {
            history.replace(`/characters`)
        } else {
            url.searchParams.set('filters', JSON.stringify(formParams));
            if (typeof formParams === 'object' && formParams) {
                if (!Object.keys(formParams)) {
                    url.searchParams.delete('filters');
                }
            }
            history.replace(`/characters?${url.searchParams}`)
        }
    }, [formParams])

    return (
        <Form onSubmit={(values) => console.log(values)}>
            {(reactFinalProps): ReactElement => (
                <form onSubmit={reactFinalProps.handleSubmit}>
                    <FormSpy
                        subscription={{values: true}}
                        onChange={(form) => {
                            console.log(form);
                            dispatch(charactersPageActions.setFormFilters({...formParams, input: form.values.inputSearch}));
                        }}
                    />
                    <Field name={'inputSearch'}>
                        {(fieldProps): ReactElement => (
                            <Input
                                id={'inputSearch'}
                                inputStyles={
                                    { width: `${TRANSLATE_CHARACTERS_LIST}vw`, backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                                }
                                {...fieldProps.input}
                                placeholder={'Поиск'}
                            />)}
                    </Field>
                    <SelectContainer>
                        <Select
                            id={'gender'}
                            title={'Пол'}
                            selectOptionList={gender}
                            selectStyles={
                                { backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                            }
                            selectDropdownStyles={
                                { backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                            }
                            selectImg={arrow}
                        />
                        <Select
                            id={'race'}
                            title={'Раса'}
                            selectOptionList={race}
                            selectStyles={
                                { backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                            }
                            selectDropdownStyles={
                                { backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                            }
                            selectImg={arrow}
                        />
                        <Select
                            id={'side'}
                            title={'Сторона'}
                            selectOptionList={side}
                            selectStyles={
                                { backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                            }
                            selectDropdownStyles={
                                { backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR }
                            }
                            selectImg={arrow}
                        />
                        <NewCharButton onClick={(event) => dispatch(charactersPageActions.toggleNewModalOpen(true))}> Добавить + </NewCharButton>
                    </SelectContainer>
                </form>
            )}
        </Form>
    );
}

export default SearchForm;
