import styled from "styled-components";

export const CharacterPageContainer = styled.div<{background: string}>`
  background-image: url(${props => props.background});
  background-size: 100% 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  overflow-y: hidden;
`;
