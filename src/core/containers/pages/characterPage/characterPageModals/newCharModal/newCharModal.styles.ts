import styled from "styled-components";
import {FILTER_FIELDS_BACKGROUND_COLOR} from "../../../../../../styles/styles.constants";
import {CSSProperties} from "react";

export const NewCharForm = styled.div`
  width: 50%;
  border-right: 2px solid #494949;
  color: #B09A81;
  display: flex;
  flex-direction: column;
  gap: 10px;
  font-size: 14px;
  padding: 2rem;
`;

export const ColorPicker = styled.input`
  width: 50px;
  border: none;
`;

export const FilePicker = styled.input`
  opacity: 0;
  position: absolute;
  z-index: -1;
`;

export const FilePickerContainer = styled.div`
  width: 200px;
  background-color: ${FILTER_FIELDS_BACKGROUND_COLOR};
  display: flex;
  align-items: center;
  gap: 15px;
  flex-direction: column;
  border-radius: 4px;
  margin-bottom: 9px;
`;

export const ImgContainer = styled.div`
  width: 200px;
  height: 200px;
  position: relative;
  background-color: ${FILTER_FIELDS_BACKGROUND_COLOR};
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const ImgURL = styled.input`
    height: 12px;
    background-color: ${FILTER_FIELDS_BACKGROUND_COLOR};
    width: 80%;
    background: inherit;
    border: 1px solid #462929;
    border-radius: 4px;
    padding: 3px;
    font-size: 14px;
    margin-bottom: 10px;
    &::-webkit-input-placeholder { 
      color: #462929; 
    }
    &:focus {
      outline: none;
    }
`;

export const editImg: CSSProperties = {
    height: '29px',
    width: '85px',
    background: '#B09A81',
    borderRadius: '4px',
    border: 'none',
    fontSize: '14px',
    color: '#462929',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
};

export const SaveImgButton = styled.button`
  height: 29px;
  width: 85px;
  color: #B09A81;
  font-size: 14px;
  background: #1F4F3B;
  border-radius: 4px;
  border: none;
  align-self: flex-end;
`;
