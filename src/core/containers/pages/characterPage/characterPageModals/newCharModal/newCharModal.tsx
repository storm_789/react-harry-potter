import {FC, ReactElement, useState} from "react";
import {NewCharForm, SaveImgButton} from "./newCharModal.styles";
import {useAppDispatch, useAppSelector} from "../../../../../../store/store.hooks";
import {FormSpy, Form} from "react-final-form";
import {NewCharModalFields} from "./newCharModal.type";
import NewCharModalTextField from "./newCharModalComponents/newCharModalTextField";
import NewCharModalTextarea from "./newCharModalComponents/newCharModalTextaria";
import InfoContainer from "./newCharModalSections/infoContainer";
import FilePicker from "./newCharModalSections/filePicker";
import ColorPicker from "./newCharModalSections/colorPicker";
import Modal from "../../../../../components/modal/modal";
import {ICharacter} from "../../../../../../components/registry/registry.type";
import CharInfoById from "../charInfoByIdModal/charInfoById";
import Card from "../../../../../../components/card/card";
import {FILTER_FIELDS_BACKGROUND_COLOR} from "../../../../../../styles/styles.constants";
import Pagination from "../../../../../../components/pagination/pagination";
import prev from '../../../../../../styles/img/prev.png';
import next from '../../../../../../styles/img/next.png';
import {addNewCharacter} from "../../../../../../services/characterService/character.service";
import {newCharacterFactory} from "../../../../../factory/character.factory";
import {charactersPageActions} from "../../../../../../store/characters/characters.slice";

const NewCharModal: FC = () => {

    const dispatch = useAppDispatch();

    const [currentPage, setCurrentPage] = useState<number>(0);
    const [newCharacter, setNewCharacter] = useState<ICharacter>({
        id: '',
        firstName: '',
        lastName: '',
        cardInfo: {
            gender: '',
            race: '',
            side: ''
        },
        img: '',
        mainInfoColor: '',
        cardInfoColor: '',
        cardInfoBackGroundColor: ''
    });

    const initialFormValues = {
        name: '',
        gender: '',
        race: '',
        side: '',
        imgURL: '',
        mainInfoColor: '#000000',
        cardInfoColor: '#000000',
        cardInfoBackGroundColor: '#000000',
        description: '',
        tags: '',
    }

    const previewList = [
        {
            id: 0,
            value: [
                <div style={{width: '100%', height: '350px', backgroundColor: '#252020', marginTop: '5%', marginBottom: '25px'}} key={0}>
                    <CharInfoById
                        character={newCharacter}
                        mainInfoStyles={{
                            fontSize: '20px',
                            lineHeight: '25px',
                            color: newCharacter.mainInfoColor,
                            marginBottom: '15px'
                        }}
                        cardInfoStyles={{width: '100%', fontSize: '15px', lineHeight: '25px', padding: '3px'}}
                        cardInfoParamStyle={{lineHeight: '30px'}}
                    />
                </div>
            ]
        },
        {
            id: 1,
            value: [
                <div style={{width: '100%', marginTop: '5%', marginBottom: '25px'}} key={1}>
                    <Card
                        id={newCharacter.id}
                        mainInfoStyles={{backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR, height: '70%', padding: '0'}}
                        containerStyles={{width: '50%', height: '350px', transform: 'translateX(50%)'}}
                        character={{
                            firstName: newCharacter.firstName,
                            lastName: newCharacter.lastName,
                            img: newCharacter.img,
                            mainInfoColor: newCharacter.mainInfoColor,
                        }}
                        cardInfoProps={{
                            cardInfoColor: newCharacter.cardInfoColor,
                            cardInfoBackGroundColor: newCharacter.cardInfoBackGroundColor,

                            cardInfoStyles: {width: '100%', fontSize: '15px', lineHeight: '25px', padding: '1px', height: '30%'},
                            cardInfoParamStyles: {lineHeight: '30px', padding: '2px'},
                            ...newCharacter.cardInfo
                        }}
                    />
                </div>
            ]
        },
    ];

    const saveChar = async () => {
        await addNewCharacter(newCharacterFactory(newCharacter));
        dispatch(charactersPageActions.fetchCurrentCharacterList());
        dispatch(charactersPageActions.toggleNewModalOpen(false));
    }

    return (
        <Modal>
            <div style={{display: 'flex'}}>
                <Form onSubmit={saveChar} initialValues={initialFormValues}>
                    {(reactFinalProps): ReactElement => (
                        <form
                            onSubmit={reactFinalProps.handleSubmit}
                            style={{
                                width: '50%',
                                borderRight: '2px solid #494949',
                                color: '#B09A81',
                                display: 'flex',
                                flexDirection: 'column',
                                gap: '10px',
                                fontSize: '14px',
                                padding: '10px',
                            }}
                        >
                            <FormSpy
                                subscription={{values: true}}
                                onChange={(form) => {
                                    const name = form.values.name ? form.values.name.split(' ') : '';
                                    setNewCharacter(prevState => ({
                                        ...prevState,
                                        firstName: name[0] ? name[0] : '',
                                        lastName: name[1] ? name[1] : '',
                                        cardInfo: {
                                            gender: form.values.gender,
                                            race: form.values.race,
                                            side: form.values.side,
                                        },
                                        img: form.values.imgURL,
                                        mainInfoColor: form.values.mainInfoColor,
                                        cardInfoColor: form.values.cardInfoColor,
                                        cardInfoBackGroundColor: form.values.cardInfoBackGroundColor,
                                        description: form.values.description,
                                        tags: form.values.tags ? form.values.tags.split(',') : []
                                    }))
                                }}
                            />
                            <NewCharModalTextField caption={'Добавить имя'} fieldName={NewCharModalFields.Name}>

                            </NewCharModalTextField>
                            <InfoContainer/>
                            <NewCharModalTextarea
                                maxLength={100}
                                caption={'Добавить описание'}
                                fieldName={NewCharModalFields.Description}/>
                            <NewCharModalTextField caption={'Добавить теги'} fieldName={NewCharModalFields.Tags}/>
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                <FilePicker/>
                                <ColorPicker/>
                            </div>
                            <SaveImgButton type={'submit'}>Сохранить</SaveImgButton>
                        </form>
                    )}
                </Form>
                <div style={{width: '50%', marginTop: '10%', padding: '10px'}}>
                    {previewList[currentPage].value}
                    <Pagination
                        paginationList={previewList}
                        currentPage={currentPage}
                        setCurrentPage={setCurrentPage}
                        prevButtonImg={prev}
                        nextButtonImg={next}
                    />
                </div>
            </div>
        </Modal>
    );
}

export default NewCharModal;
