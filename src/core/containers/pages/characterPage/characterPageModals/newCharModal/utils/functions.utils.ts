export const tagsLength = (tagsString: string): number => {
    return tagsString.length ? tagsString.split(',').length : 0;
}
