import {CSSProperties, Dispatch, ReactElement, SetStateAction} from "react";

export enum NewCharModalFields {
    Name = 'name',
    Gender = 'gender',
    Race = 'race',
    Side = 'side',
    Description = 'description',
    Tags = 'tags',
    Img = 'img',
    ImgURL = 'imgURL',
    EditImg = 'editImg',
    MainInfoColor = 'mainInfoColor',
    CardInfoColor = 'cardInfoColor',
    CardInfoBackGroundColor = 'cardInfoBackGroundColor',
}

export interface IInputProps {
    fieldName: string;
    caption: string;
    labelStyles?: CSSProperties;
    maxTags?: number;
}

export interface ITextariaProps {
    fieldName: string;
    caption: string;
    textareaStyles?: CSSProperties;
    maxLength?: number;
}

export interface IFilePicker {
    fieldId: string;
    fieldName: string;
    labelContent: JSX.Element | string;
    setIsImgLoad: Dispatch<SetStateAction<boolean>>;
    setCurrentImg: Dispatch<SetStateAction<string>>;
    labelStyles?: CSSProperties;
}

export interface IURLImg {
    fieldName: string;
    setIsImgLoad: Dispatch<SetStateAction<boolean>>;
    setCurrentImg: Dispatch<SetStateAction<string>>;
}


