import {FC, ReactElement} from "react";
import Input from "../../../../../../../components/formInput/input";
import {FILTER_FIELDS_BACKGROUND_COLOR} from "../../../../../../../styles/styles.constants";
import {Field} from "react-final-form";
import {IInputProps, NewCharModalFields} from "../newCharModal.type";
import {tagsLength} from "../utils/functions.utils";

const NewCharModalTextField: FC<IInputProps> = (props) => {
    const {maxTags = 3} = props;
    const required = (value: any) => {
        if (value) {
            switch (props.fieldName) {
                case 'gender':
                    if ( value === 'Мужчина' ||  value === 'Женщина' ||  value === 'Неизвестен' ) {
                        return;
                    } else return 'Недопустимо!';
                case 'side':
                    if ( value === 'Добро' ||  value === 'Зло' ||  value === 'Неизвестен' ) {
                        return;
                    } else return 'Недопустимо!';
                case 'race':
                    if ( value === 'Человек' ||  value === 'Получеловек' ||  value === 'Неизвестен' ) {
                        return;
                    } else return 'Недопустимо!'
                default:
                    return;
            }
        } else return "Обязательно";
    };

    return (
        <Field name={props.fieldName} validate={required}>
            {(fieldProps): ReactElement => (
                <label style={props.labelStyles}>
                    {
                        props.fieldName === NewCharModalFields.Tags ?
                            <div style={{display: 'flex', justifyContent: 'space-between'}}>
                                <span>{props.caption}</span>
                                <span style={{color: '#494949'}}>{tagsLength(fieldProps.input.value)}/{maxTags}</span>
                            </div>
                        : props.caption
                    }
                    <Input
                        id={props.fieldName}
                        inputStyles={
                            { width: `calc(100% - 8px)`, height: '30px', backgroundColor: FILTER_FIELDS_BACKGROUND_COLOR , fontSize: 'inherit'}
                        }
                    />
                </label>
            )}
        </Field>
    )
}

export default NewCharModalTextField;
