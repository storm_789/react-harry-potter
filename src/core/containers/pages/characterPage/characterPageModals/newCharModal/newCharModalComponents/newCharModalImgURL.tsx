import {FC, ReactElement} from "react";
import {Field} from "react-final-form";
import {IURLImg} from "../newCharModal.type";
import {ImgURL} from "../newCharModal.styles";

const NewCharModalImgURL: FC<IURLImg> = (props) => {
    return (
        <Field name={props.fieldName}>
            {(fieldProps): ReactElement => (
                <ImgURL
                    {...fieldProps.input}
                    type='text'
                    placeholder={'URL изображения...'}
                    onChange={(event) => {
                        props.setIsImgLoad(true);
                        props.setCurrentImg(event.target.value);
                        fieldProps.input.onChange(event)
                    }}
                />
            )}
        </Field>
    );
}

export default NewCharModalImgURL;
