import {FC, ReactElement} from "react";
import {Field} from "react-final-form";
import {IFilePicker} from "../newCharModal.type";
import {FilePicker} from "../newCharModal.styles";

const NewCharModalFilePicker: FC<IFilePicker> = (props) => {

    const loadImg = (event: any) => {
        let selectedFile = event.target.files[0];
        let reader = new FileReader();

        reader.onload = function(event) {
            props.setIsImgLoad(true);
            typeof event.target!.result === 'string' ?
                props.setCurrentImg(event.target!.result) : props.setCurrentImg('');
        };

        reader.readAsDataURL(selectedFile);
    }

    return (
        <>
            <label htmlFor={props.fieldId} style={{cursor: 'pointer', ...props.labelStyles}}>
                {props.labelContent}
            </label>
            <Field name={props.fieldName}>
                {(fieldProps): ReactElement => (
                    <FilePicker
                        {...fieldProps.input}
                        id={props.fieldId}
                        type='file'
                        onChange={(event) => {
                            loadImg(event);
                            fieldProps.input.onChange(event)
                        }}
                    />
                )}
            </Field>
        </>
    );
}

export default NewCharModalFilePicker;
