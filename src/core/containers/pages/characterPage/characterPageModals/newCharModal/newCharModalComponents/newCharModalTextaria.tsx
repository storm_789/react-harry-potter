import {FC, ReactElement} from "react";
import {FILTER_FIELDS_BACKGROUND_COLOR} from "../../../../../../../styles/styles.constants";
import {Field} from "react-final-form";
import {ITextariaProps} from "../newCharModal.type";
import styled from "styled-components";

const StyledTextaria = styled.textarea<{isError?: boolean}>`
  width: calc(100% - 8px);
  height: 150px;
  background-color: ${FILTER_FIELDS_BACKGROUND_COLOR};
  resize: none;
  border: ${props => props.isError ? '2px solid red' : 'none'};
  padding: 8px 16px;
  font-size: inherit;
  box-sizing: border-box;
  border-radius: 6px;
  &:focus {
    outline: none;
  }
`;

const NewCharModalTextarea: FC<ITextariaProps> = (props) => {
    const {maxLength = 100} = props;
    const required = (value: any) => {
        return value ? undefined : "Обязательно"
    };
    return (
        <Field name={props.fieldName} validate={required}>
            {(fieldProps): ReactElement => (
                <label style={{display: 'flex', flexDirection: 'column'}}>
                    <div style={{display: 'flex', justifyContent: 'space-between'}}>
                        <span>{props.caption}</span>
                        <span style={{color: '#494949'}}>{fieldProps.input.value.length}/{maxLength}</span>
                    </div>
                    <StyledTextaria
                        maxLength={maxLength}
                        {...fieldProps.input}
                        {...fieldProps.meta}
                        isError={fieldProps.meta.error && fieldProps.meta.touched}
                    />
                    {fieldProps.meta.error && fieldProps.meta.touched && <span style={{color: 'red'}}>{fieldProps.meta.error}</span>}
                </label>

            )}
        </Field>
    )
}

export default NewCharModalTextarea;
