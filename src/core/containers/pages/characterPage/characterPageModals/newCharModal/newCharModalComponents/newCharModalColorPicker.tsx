import {CSSProperties, FC, ReactElement} from "react";
import {Field} from "react-final-form";
import {ColorPicker} from "../newCharModal.styles";
import {IInputProps} from "../newCharModal.type";

const NewCharModalColorPicker: FC<IInputProps> = (props) => {
    return (
        <label style={{display: 'flex', alignItems: 'center', ...props.labelStyles}}>
            <Field name={props.fieldName}>
                {(fieldProps): ReactElement => (
                    <ColorPicker {...fieldProps.input} type='color'/>
                )}
            </Field>
            <span style={{marginLeft: '2px'}}>{props.caption}</span>
        </label>
    )
}

export default NewCharModalColorPicker;
