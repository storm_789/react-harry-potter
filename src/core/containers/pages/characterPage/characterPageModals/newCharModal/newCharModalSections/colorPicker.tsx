import {FC} from "react";
import {NewCharModalFields} from "../newCharModal.type";
import NewCharModalColorPicker from "../newCharModalComponents/newCharModalColorPicker";

const ColorPicker: FC = () => {
    return (
        <div style={{display: 'flex', flexDirection: 'column', gap: '5px'}}>
            <span>Выбрать цвет</span>
            <NewCharModalColorPicker fieldName={NewCharModalFields.MainInfoColor} caption={'Цвет имени'}/>
            <NewCharModalColorPicker fieldName={NewCharModalFields.CardInfoBackGroundColor} caption={'Цвет фона параметров'}/>
            <NewCharModalColorPicker fieldName={NewCharModalFields.CardInfoColor} caption={'Цвет параметров'}/>
        </div>
    );
}

export default ColorPicker;
