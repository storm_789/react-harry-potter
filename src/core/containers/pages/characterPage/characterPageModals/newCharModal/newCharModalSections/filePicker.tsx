import {FC, useRef, useState} from "react";
import NewCharModalTextField from "../newCharModalComponents/newCharModalTextField";
import {NewCharModalFields} from "../newCharModal.type";
import {editImg, FilePickerContainer, ImgContainer, SaveImgButton} from "../newCharModal.styles";
import NewCharModalFilePicker from "../newCharModalComponents/newCharModalFilePicker";
import addImg from "../../../../../../../styles/img/add.png";
import NewCharModalImgURL from "../newCharModalComponents/newCharModalImgURL";

const FilePicker: FC = () => {

    const [isImgLoad, setIsImgLoad] = useState<boolean>(false);
    const [currentImg, setCurrentImg] = useState<string>('');

    return (
        <div>
            <span>Добавить фото</span>
            <FilePickerContainer>
                <ImgContainer>
                    {
                        isImgLoad ?
                            <img src={currentImg} style={{position: "absolute", width: '100%', height: '100%'}}/>
                        : null
                    }
                </ImgContainer>
                <NewCharModalImgURL
                    fieldName={NewCharModalFields.ImgURL}
                    setCurrentImg={setCurrentImg}
                    setIsImgLoad={setIsImgLoad}
                />
            </FilePickerContainer>
        </div>
    );
}

export default FilePicker;
