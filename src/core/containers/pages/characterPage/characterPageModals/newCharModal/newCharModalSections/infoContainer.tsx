import {FC} from "react";
import NewCharModalTextField from "../newCharModalComponents/newCharModalTextField";
import {NewCharModalFields} from "../newCharModal.type";

const InfoContainer: FC = () => {
    return (
        <div style={{display: 'flex', gap: '10%'}}>
            <NewCharModalTextField caption={'Пол'} fieldName={NewCharModalFields.Gender} labelStyles={{display: 'flex', flexDirection: 'column'}}/>
            <NewCharModalTextField caption={'Раса'} fieldName={NewCharModalFields.Race} labelStyles={{display: 'flex', flexDirection: 'column'}}/>
            <NewCharModalTextField caption={'Сторона'} fieldName={NewCharModalFields.Side} labelStyles={{display: 'flex', flexDirection: 'column'}}/>
        </div>
    );
}

export default InfoContainer;
