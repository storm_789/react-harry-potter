import {FC, useEffect} from "react";
import {useHistory, useParams} from "react-router-dom";
import Modal from "../../../../../components/modal/modal";

import {useAppDispatch, useAppSelector} from "../../../../../../store/store.hooks";
import {charactersPageActions} from "../../../../../../store/characters/characters.slice";
import CharInfoById from "./charInfoById";


const CharInfoByIdModal: FC = () => {
    const { id } = useParams<{id:string}>();
    const cardId = id ? id : localStorage.getItem('cardId')!;
    const history = useHistory();

    const dispatch = useAppDispatch();
    const character = useAppSelector(state => state.characters.selectedCard);

    useEffect(() => {
        if (!id) {
            history.replace(`/characters/${cardId}`);
        }
    }, []);

    useEffect(() => {
        dispatch(charactersPageActions.fetchSelectedCard(cardId));
    }, []);

    return (
        <Modal>
            {
                character ? <CharInfoById character={character}/> : null
            }
        </Modal>
    )
}

export default CharInfoByIdModal;
