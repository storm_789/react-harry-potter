import styled, {CSSObject} from "styled-components";

export const ModalContainer = styled.div`
    position: absolute;
    top: 0;
    left: 0;
    width: 100vw ;
    height: calc(100% - 50px);
    padding-top: 50px;
    background-color: rgba(0, 0, 0, 0.3);
    color: black;
    overflow-y: scroll;
    padding-left: 50vw;
    z-index: 2000;
    &::-webkit-scrollbar {
      width: 0;
    }
`;

export const Modal = styled.div`
    position: relative;
    overflow-x: hidden;
    max-width: 65vw;
    background: linear-gradient(180deg, #2A2C35 52.6%, #000000 100%);
    transform: translateX(-50%);
    padding: 0;
    display: flex;
    height: 90%;
    &::-webkit-scrollbar {
      width: 0;
    }
`;

export const ContentContainer = styled.div`
  width: 40%;
  padding: 50px 30px;
`;

export const MainInfo = styled.h3<{mainInfoStyle?: CSSObject}>`
  color: ${props => props.mainInfoStyle?.color ? props.mainInfoStyle?.color : 'white'};
  font-weight: 500;
  font-size: 50px;
  line-height: 50px;
  margin-bottom: 48px;
  
  ${props => props.mainInfoStyle}
`;

export const Description = styled.p`
  color: white;
  font-weight: normal;
  font-size: 15px;
  line-height: 25px;
`;

export const ImgContainer = styled.div<{background: string}>`
  width: 60%;
  height: 100%;
  display: flex;
  background-image: url(${props => props.background});
  background-size: 100% 100%;
  position: relative;
`;

export const TagsContainer = styled.ul`
  position: absolute;
  bottom: 44px;
  right: 50px;
  color: white;
`;

export const Tag = styled.li<{ tagColor: string }>`
  background-color: ${props => props.tagColor};
  margin-top: 8px;
  font-size: 14px;
  line-height: 25px;
  padding: 3px 21px;
  text-align: center;
  border-radius: 8px;
`;
