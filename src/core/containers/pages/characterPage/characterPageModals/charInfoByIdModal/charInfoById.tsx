import {FC} from "react";
import {ContentContainer, Description, ImgContainer, MainInfo, Tag, TagsContainer} from "./charInfoById.styles";
import CardInfo from "../../../../../../components/card/cardInfo/cardInfo";
import {ICharInfoById, TagsColor} from "./cardInfoById.type";

const CharInfoById: FC<ICharInfoById> = (props) => {
    const { mainInfoStyles, character, cardInfoStyles, cardInfoParamStyle } = props;

    return (
        <div style={{display: 'flex', height: '100%'}}>
            <ContentContainer>
                <MainInfo mainInfoStyle={mainInfoStyles}>{`${character.firstName} ${character.lastName}`}</MainInfo>
                <CardInfo
                    gender={character.cardInfo.gender}
                    cardInfoColor={character.cardInfoColor}
                    race={character.cardInfo.race}
                    side={character.cardInfo.side}
                    cardInfoBackGroundColor={character.cardInfoBackGroundColor}
                    cardInfoStyles={{borderRadius: '4px', marginBottom: '23px', ...cardInfoStyles}}
                    cardInfoParamStyles={{...cardInfoParamStyle}}
                />
                <Description>{character.description}</Description>
            </ContentContainer>
            <ImgContainer background={character.img}>
                <TagsContainer>
                    {character.tags?.map((tag, idx) => (
                            <Tag tagColor={TagsColor[idx]} key={idx.toString()}>{tag}</Tag>
                        )
                    )}
                </TagsContainer>
            </ImgContainer>
        </div>
    );
}

export default CharInfoById;
