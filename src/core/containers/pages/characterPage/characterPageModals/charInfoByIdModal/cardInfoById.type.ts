import {ICharacter} from "../../../../../../components/registry/registry.type";
import {CSSObject} from "styled-components";

export enum TagsColor {
    '#26514E',
    '#123856',
    '#512C38',
}

export interface ICharInfoById {
    character: ICharacter;
    mainInfoStyles?: CSSObject;
    cardInfoStyles?: CSSObject;
    cardInfoParamStyle?: CSSObject;
}
