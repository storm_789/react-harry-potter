export enum MainContent {
    MainHeading = 'Найди любимого персонажа “Гарри Поттера”',
    SubHeading = 'Вы сможете узнать тип героев, их способности, сильные стороны и недостатки.',
    ButtonLabel = 'Начать'
}
