import {FC} from "react";

import background from '../../../../styles/img/bg.png';
import {
    LARGE_BUTTON_COLOR,
    LARGE_BUTTON_HOVER_COLOR,
    LARGE_BUTTON_PRESSED_COLOR,
} from '../../../../styles/styles.constants';
import StartButton from "../../../../components/button/button";
import {Main} from "./mainPageStyles/mainPage.styles";
import {MainContent} from "./mainPage.type";


const MainPage: FC = () => {
    return (
        <Main background={background}>
            <div style={{display: 'flex', flexDirection: 'column', padding: '50px 0 0 100px'}}>
                <h1>{MainContent.MainHeading}</h1>
                <h2>{MainContent.SubHeading}</h2>
                <StartButton
                    hoverColor={LARGE_BUTTON_HOVER_COLOR}
                    mainColor={LARGE_BUTTON_COLOR}
                    pressedColor={LARGE_BUTTON_PRESSED_COLOR}
                    path={'characters'}
                >
                    {MainContent.ButtonLabel}
                </StartButton>
            </div>
        </Main>
    );
};

export default MainPage;
