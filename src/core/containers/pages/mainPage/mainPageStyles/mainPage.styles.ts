import styled from "styled-components";

export const Main = styled.main<{ background: string }>`
  background-image: url(${props => props.background});
  background-size: 100% 100%;
  height: 100%;
  
  h1 {
    font-size: 72px;
    line-height: 80px;
    color: white;
    margin-bottom: 65px;
    width: 711px;
  }
  
  h2 {
    fontSize: 24px;
    line-height: 31px;
    color: white;
    margin-bottom: 69px;
    width: 549px;
  }
`;
