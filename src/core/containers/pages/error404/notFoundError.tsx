import {FC} from "react";
import background from '../../../../styles/img/404.png';
import {
    LARGE_BUTTON_COLOR,
    LARGE_BUTTON_HOVER_COLOR,
    LARGE_BUTTON_PRESSED_COLOR,
} from '../../../../styles/styles.constants';
import StartButton from "../../../../components/button/button";
import {Text404} from './notFoundPage.type';
import {TextContainer} from "./notFoundPage.styles";


const NotFoundPage: FC = () => {
    return (
        <TextContainer background={background}>
            <h1>{Text404.Code}</h1>
            <h2>{Text404.Text}</h2>
            <StartButton
                hoverColor={LARGE_BUTTON_HOVER_COLOR}
                mainColor={LARGE_BUTTON_COLOR}
                pressedColor={LARGE_BUTTON_PRESSED_COLOR}
                path={'/'}
            >
                {Text404.ButtonLabel}
            </StartButton>
        </TextContainer>
    );
};

export default NotFoundPage;
