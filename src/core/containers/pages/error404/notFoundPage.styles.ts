import styled from "styled-components";

export const TextContainer = styled.div<{background: string}>`
  background-image: url(${props => props.background});
  background-size: 100% 100%;
  padding: 21vh 0 51vh 0;
  text-align: center;
  h1 {
    font-size: 72px;
    line-height: 80px;
    color: white;
    margin-bottom: 65px;
  
  }

  h2 {
    fontSize: 24px;
    line-height: 31px;
    color: white;
    margin-bottom: 69px;
  }
  
`;
