import styled from "styled-components";

export const ModalContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: calc(100% - 15px);
  margin-top: 15px;
  background-color: rgba(0, 0, 0, 0.7);
  color: black;
  overflow-y: scroll;
  padding-left: 50vw;
  z-index: 2000;
  &::-webkit-scrollbar {
    width: 0;
  }
`;

export const ModalContent = styled.div`
  position: relative;
  overflow: scroll;
  width: 70vw;
  height: 100%;
  background: linear-gradient(180deg, #2A2C35 52.6%, #000000 100%);
  transform: translateX(-50%);
  &::-webkit-scrollbar {
    width: 0;
  }
`;

export const Close = styled.button`
  z-index: 5;
  padding: 0;
  background: none;
  border: none;
  cursor: pointer;
  position: absolute;
  top: 2rem;
  right: 2rem;
`;
