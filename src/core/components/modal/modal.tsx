import {FC, useState} from "react";
import {charactersPageActions} from "../../../store/characters/characters.slice";
import {Close, ModalContainer, ModalContent} from "./modal.styles";
import {useAppDispatch} from "../../../store/store.hooks";

const Modal: FC = ({ children}) => {

    const dispatch = useAppDispatch();

    const closeModal = () => {
        dispatch(charactersPageActions.toggleNewModalOpen(false));
        dispatch(charactersPageActions.toggleCharInfoModalOpen(false));
        dispatch(charactersPageActions.clearSelectedCard());
    }

    return (
        <ModalContainer onClick={(event) => closeModal()}>
            <ModalContent onClick={(event) => event.stopPropagation()}>
                <Close onClick={(event) => closeModal()}>
                    <img src="../../../img/icon_close.png" alt=""/>
                </Close>
                {
                    children
                }
            </ModalContent>
        </ModalContainer>
    )
}

export default Modal;
