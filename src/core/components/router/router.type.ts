export enum RouterPaths {
    Main = '/',
    NewChar = '/new',
    CharPage = '/characters',
    SelectedChar = '/characters/:id',
    NotFound = '/404',
}
