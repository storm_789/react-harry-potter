import {Switch, Route, Redirect} from "react-router-dom";
import MainPage from "../../containers/pages/mainPage/mainPage";
import React from "react";
import CharacterPage from "../../containers/pages/characterPage/characterPage";
import {RouterPaths} from "./router.type";
import NotFoundPage from "../../containers/pages/error404/notFoundError";

export const RouteSwitcher = (): React.ReactElement => {
    return (
        <Switch>
            <Route exact path={RouterPaths.Main}>
                <MainPage />
            </Route>
            <Route exact path={RouterPaths.NewChar}>
                <CharacterPage />
            </Route>
            <Route exact path={RouterPaths.CharPage}>
                <CharacterPage />
            </Route>
            <Route exact path={RouterPaths.SelectedChar}>
                <CharacterPage />
            </Route>
            <Route exact path={RouterPaths.NotFound}>
                <NotFoundPage />
            </Route>
            <Redirect to={RouterPaths.NotFound}/>
        </Switch>
    );
}
