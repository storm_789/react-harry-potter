import {IOptions} from "../../store/dictionaries/dictionaries.type";
import {ISelectModel} from "../../services/selectService/select.model";

export const dictionariesFactory = (model: ISelectModel[]): IOptions[] => {
    return model.map(field => ({
        id: field.id,
        value: field.value,
    }));
};
