import {ICharacterByIdModel, ICharacterContentList} from "../../services/characterService/characters.model";
import {ICharacter} from '../../components/registry/registry.type';
import {TAppState} from "../../store/store.type";
import {store} from "../../store";

export const characterFactory = (model: ICharacterContentList[]): ICharacter[] => {
    return model.map(char => ({
        id: char.id,
        img: char.imageURL,
        firstName: char.name.split(' ')[0],
        lastName: char.name.split(' ')[1],
        cardInfo: {
            gender: char.gender,
            side: char.side,
            race: char.race
        },
        mainInfoColor: char.nameColor,
        cardInfoColor: char.parametersColor,
        cardInfoBackGroundColor: char.backgroundColor,
    }));
}

export const characterByIdFactory = (model: ICharacterByIdModel): ICharacter => {
    return (
        {
            id: model.id,
            img: model.imageURL,
            firstName: model.name.split(' ')[0],
            lastName: model.name.split(' ')[1],
            cardInfo: {
                gender: model.gender.value,
                side: model.side.value,
                race: model.race.value
            },
            mainInfoColor: model.nameColor,
            cardInfoColor: model.parametersColor,
            cardInfoBackGroundColor: model.backgroundColor,
            description: model.description,
            tags: [model.tag1, model.tag2, model.tag3]
        }
    );
}

export const newCharacterFactory = (char: ICharacter): any => {
    const state = store.getState();
    const getGenderId = (state: TAppState) => state.dictionary.gender;
    const getRaceId = (state: TAppState) => state.dictionary.race;
    const getSideId = (state: TAppState) => state.dictionary.side;

    return (
        {
            name: `${char.firstName} ${char.lastName}`,
            description: char.description ? char.description : '',
            imageURL: char.img,
            nameColor: char.mainInfoColor,
            backgroundColor: char.cardInfoBackGroundColor,
            parametersColor: char.cardInfoColor,
            tag1: char.tags ? char.tags[0] : '',
            tag2: char.tags ? char.tags[1] ? char.tags[1] : '' : '',
            tag3: char.tags ? char.tags[2] ? char.tags[2] : '' : '',
            gender: {
                id: getGenderId(state).find(gender => gender.value === char.cardInfo.gender)!.id,
                value: char.cardInfo.gender,
            },
            side: {
                id: getSideId(state).find(side => side.value === char.cardInfo.side)!.id,
                value: char.cardInfo.side,
            },
            race: {
                id: getRaceId(state).find(race => race.value === char.cardInfo.race)!.id,
                value: char.cardInfo.race,
            },
            sideId: getSideId(state).find(side => side.value === char.cardInfo.side)!.id,
            raceId: getRaceId(state).find(race => race.value === char.cardInfo.race)!.id,
            genderId: getGenderId(state).find(gender => gender.value === char.cardInfo.gender)!.id
        }
    );
}
