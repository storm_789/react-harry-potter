import styled, {CSSObject} from "styled-components";

export const CharacterRegistry = styled.div<{registryStyles?: CSSObject}>`
  width: 65vw;
  height: 100%;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  margin-top: 5px;
  
  ${props => props.registryStyles},
`;

export const CharacterList = styled.ul`
  display: flex;
  gap: 1vw;
  height: 90%;
  transition: .5s;
`;
