import {CSSObject} from "styled-components";

export interface ICardInfo {
    gender: string;
    race: string;
    side: string;
}

export interface ICharacter {
    id: string;
    img: string;
    firstName: string;
    lastName: string;
    cardInfo: ICardInfo;
    mainInfoColor: string;
    cardInfoColor: string;
    cardInfoBackGroundColor: string;
    description?: string;
    tags?: string[];
}

export interface IRegistry {
    registryProps: {
        registryList: ICharacter[];
        registryStyles?: CSSObject;
        maxListLength: number;
    };
}

export interface IPaginationList {
    id: number;
    value: ICharacter[] | JSX.Element[];
}
