import React, {FC, useEffect, useState} from "react";
import {ICharacter, IPaginationList, IRegistry} from "./registry.type";
import {CharacterList, CharacterRegistry} from "./registryStyles/registry.styles";
import Card from "../card/card";
import Pagination from "../pagination/pagination";
import prev from '../../styles/img/prev.png';
import next from '../../styles/img/next.png';

const Registry: FC<IRegistry> = (props)=> {
    const { registryProps } = props;
    const { registryStyles, registryList } = registryProps;

    const [paginationList, setPaginationList] = useState<IPaginationList[]>([]);
    const [currentPage, setCurrentPage] = useState<number>(0);

    useEffect(() => {
        setPaginationList([]);
        for (let i = 0, j = 0; i < registryList.length; i += 3, j++) {
            setPaginationList((prev)  => {
                    return [...prev, { id: j, value: registryList.slice(i, i + 3)}]
                }
            )
        }
    }, [registryList]);


    return (
        <CharacterRegistry registryStyles={registryStyles}>
            <CharacterList>
                {
                    paginationList.length ?
                    // @ts-ignore
                    paginationList[currentPage].value.map((character: ICharacter, index: number) => {
                        return (
                            <li key={index}>
                                {
                                    <Card
                                        id={character.id}
                                        character={{
                                            firstName: character.firstName,
                                            lastName: character.lastName,
                                            img: character.img,
                                            mainInfoColor: character.mainInfoColor,
                                        }}
                                        cardInfoProps={{
                                            cardInfoColor: character.cardInfoColor,
                                            cardInfoBackGroundColor: character.cardInfoBackGroundColor,
                                            ...character.cardInfo
                                        }}
                                    />
                                }
                            </li>
                        )
                    }) : null
                }
            </CharacterList>
            <Pagination
                paginationList={paginationList}
                currentPage={currentPage}
                setCurrentPage={setCurrentPage}
                prevButtonImg={prev}
                nextButtonImg={next}
            />
        </CharacterRegistry>
    );
};

export default Registry;
