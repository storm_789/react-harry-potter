import {CSSObject} from "styled-components";

export interface INavbarItems {
    [key: string]: string
}

export interface INavbar {
    items: INavbarItems;
    liStyles?: CSSObject;
    ulStyles?: CSSObject;
}
