import {INavbar} from './navbar.type';
import {PageNavbar} from "./navbarStyles/navbar.styles";
import {FC} from "react";
import {NavLink} from 'react-router-dom'

const Navbar: FC<INavbar> = (props) => {
    const { items, liStyles, ulStyles } = props;

    return (
        <PageNavbar liStyles={liStyles} ulStyles={ulStyles}>
            <ul>
                {
                    Object.entries(items).map((item: [string, string], index) =>
                        <li key={index}><NavLink to={item[0]}>{item[1]}</NavLink></li>
                    )
                }
            </ul>
        </PageNavbar>
    );
};

export default Navbar;
