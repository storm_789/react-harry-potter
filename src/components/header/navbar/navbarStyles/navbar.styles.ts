import styled from "styled-components";
import {INavbar} from "../navbar.type";

export const PageNavbar = styled.nav<Omit<INavbar, 'items'>>`
  ul {
    display: flex; 
    ${props => props.ulStyles};
  };
  
  li {
    font-size: 20px;
    line-height: 26px;
    margin-right: 33px;
    color: #B09A81;
    cursor: pointer;
    
    a {
      text-decoration: none;
      color: #B09A81;
    }
    
    ${props => props.liStyles};
  }
`;
