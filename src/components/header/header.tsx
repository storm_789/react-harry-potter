import { IHeader } from './header.type';
import {FC} from "react";
import {PageHeader} from "./headerStyles/header.styles";
import Navbar from "./navbar/navbar";

const Header: FC<IHeader> = (props) => {
    const { headerProps, navbarProps } = props;

    return (
        <PageHeader headerStyles={headerProps.headerStyles}>
            <img src={headerProps.headerLogo} alt=""/>
            <Navbar {...navbarProps} />
        </PageHeader>
    );
};

export default Header;
