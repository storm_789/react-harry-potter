import { INavbar } from './navbar/navbar.type';
import {CSSObject} from "styled-components";

export interface IHeaderProps {
    headerStyles?: CSSObject;
    headerLogo: string;
}

export interface IHeader {
    headerProps: IHeaderProps;
    navbarProps: INavbar;
}
