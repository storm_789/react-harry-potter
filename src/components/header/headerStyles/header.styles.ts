import styled from "styled-components";
import {IHeaderProps} from "../header.type";

export const PageHeader = styled.header<Omit<IHeaderProps, 'headerLogo'>>`
  display: flex;
  align-items: center;
  background-color: black;
  padding-left: 40px;
  height: 100px;
  ${props => props.headerStyles}
`;
