import {CSSObject} from "styled-components";
import {IOptionList} from "./selectOption/selectOption.type";

export type TId = 'gender' | 'race' | 'side';

export interface ISelect {
    selectStyles?: CSSObject;
    selectDropdownStyles?: CSSObject;
    selectOptionList: IOptionList[];
    id: TId;
    selectImg: string;
    title: string;
}
