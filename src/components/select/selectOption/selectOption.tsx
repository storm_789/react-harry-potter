import React, {ChangeEvent, FC, useEffect, useState} from "react";
import {SelectOptionContainer} from "./selectOptionStyles/selectOption.styles";
import {IOptionList, ISelectOptionList, ISelectValue} from "./selectOption.type";
import {useAppDispatch, useAppSelector} from "../../../store/store.hooks";
import {charactersPageActions} from "../../../store/characters/characters.slice";

const SelectOption: FC<ISelectOptionList> = (props) => {
    const { selectDropdownStyles, id, initTitle, setSelectTitle, selectOptionList } = props;

    const dispatch = useAppDispatch();
    const formParams = useAppSelector(state => state.characters.filterFormParam);

    const [selectState, setSelectState] = useState<ISelectValue>({id: id, title: initTitle, values: []});

    useEffect(() => {
        if (selectState.values.length) {
            dispatch(charactersPageActions.setFormFilters({...formParams, [id]: [...selectState.values]}));
        } else {
            const paramsWithoutId: any = {...formParams, [id]: [...selectState.values]};
            delete paramsWithoutId[id];
            dispatch(charactersPageActions.setFormFilters(paramsWithoutId));
        }
    }, [selectState]);

    useEffect(() => {
        switch (selectState.values.length) {
            case 0:
                setSelectTitle(initTitle);
                break;
            case 1:
                setSelectTitle(selectOptionList.find(option => option.id === selectState.values[0])?.value ?? '');
                break;
            default:
                setSelectTitle(`Выбрано: ${selectState.values.length}`);
        }
    }, [selectState])

    const handleCheckboxChange = (event: ChangeEvent<HTMLInputElement>): void => {
        if (!(event.target instanceof HTMLInputElement)) {
            return;
        }

        if (event.target.checked) {
            setSelectState(prev => ({
                ...prev, values: [...prev.values, event.target.value]
            }));
        }
        else {
            setSelectState(prev => ({
                ...prev, values: [...prev.values.filter((value) => event.target.value !== value)]
            }));
        }
    };

    return (
        <SelectOptionContainer selectDropdownStyles={selectDropdownStyles}>
            {
                selectOptionList.map((option: IOptionList, index: number) =>
                    <label className='custom-checkbox' key={index}>
                        <input
                            id={id}
                            type="checkbox"
                            value={option.id}
                            onChange={handleCheckboxChange}
                        />
                        <span>{option.value}</span>
                    </label>
                )
            }
        </SelectOptionContainer>
    )
};

export default SelectOption;
