import {CSSObject} from "styled-components";
import {Dispatch, SetStateAction} from "react";

export interface IOptionList {
    id: string;
    value: string;
}

export interface ISelectOptionList {
    id: string;
    initTitle: string;
    setSelectTitle: Dispatch<SetStateAction<string>>;
    selectOptionList: IOptionList[];
    selectDropdownStyles?: CSSObject;
}

export interface ISelectValue {
    id: string;
    title?: string;
    values: string[];
}
