import styled, {CSSObject} from 'styled-components';

export const SelectOptionContainer = styled.div<{selectDropdownStyles?: CSSObject}>`
    width: 153px;
    height: 116px;
    display: flex;
    flex-direction: column;
    gap: 16px;
    padding-left: 10px;
    justify-content: center;
    position: absolute;
    top: 41px;
    left: 0;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.08);
    border-radius: 4px;
  
    ${props => props.selectDropdownStyles},
`;

