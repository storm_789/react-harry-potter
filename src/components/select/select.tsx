import {FC, useState} from "react";
import {OpenSelectButton, SelectContainer, SelectedOption} from "./selectStyles/select.styles";
import SelectOption from "./selectOption/selectOption";
import {ISelect} from "./select.type";

const Select: FC<ISelect> = (props) => {
    const { selectStyles, selectDropdownStyles, selectOptionList, selectImg, title, id } = props;

    const [selectTitle, setSelectTitle] = useState<string>(title);
    const [isSelectOpen, setIsSelectOpen] = useState<boolean>(false);

    return (
        <SelectContainer>
            <SelectedOption selectStyles={selectStyles}>
                {selectTitle}
                <OpenSelectButton
                    onClick={(event): void => {
                        event.preventDefault();
                        setIsSelectOpen(prev => !prev)
                    }}
                >
                    <img src={selectImg} alt=""/>
                </OpenSelectButton>
            </SelectedOption>
            {
                isSelectOpen ?
                    <SelectOption
                        id={id}
                        initTitle={title}
                        selectOptionList={selectOptionList}
                        setSelectTitle={setSelectTitle}
                        selectDropdownStyles={selectDropdownStyles}
                    /> : null
            }
        </SelectContainer>
    );
};

export default Select;
