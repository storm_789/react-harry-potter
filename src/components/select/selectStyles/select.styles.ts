import styled, {CSSObject} from "styled-components";

export const SelectContainer = styled.div`
  position: relative;
  z-index: 1000;
`;

export const SelectedOption = styled.div<{selectStyles?: CSSObject}>`
  width: 113px;
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0 12px;
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.08);
  border-radius: 4px;
  
  ${props => props.selectStyles},
`;

export const OpenSelectButton = styled.button`
  padding: 0;
  border: none;
  display: flex;
  background-color: inherit;
  cursor: pointer;
`;
