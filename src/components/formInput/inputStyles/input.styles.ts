import styled, {CSSObject} from "styled-components";

export const StyledInput = styled.input<{ inputStyles: CSSObject, isError?: boolean }>`
  height: 50px;
  font-family: inherit;
  font-size: 18px;
  line-height: 23px;
  padding-left: 8px;
  border: ${props => props.isError ? '2px solid red' : 'none'};
  box-shadow: 0 1px 2px rgba(50, 50, 71, 0.08);
  border-radius: 6px;
  box-sizing: border-box;
  &:focus {
    outline: none;
  }
  
  ${props => props.inputStyles}
`;
