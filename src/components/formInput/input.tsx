import {FC} from "react";
import {StyledInput} from "./inputStyles/input.styles";
import {CSSObject} from "styled-components";
import {useField} from "react-final-form";
import {NewCharModalFields} from "../../core/containers/pages/characterPage/characterPageModals/newCharModal/newCharModal.type";
import {tagsLength} from "../../core/containers/pages/characterPage/characterPageModals/newCharModal/utils/functions.utils";

const Input: FC<{inputStyles: CSSObject, placeholder?: string, id: string}> = (props) => {
    const fieldName = useField(props.id);

    return (
        <>
            <StyledInput
                type="text"
                inputStyles={props.inputStyles}
                placeholder={props.placeholder}
                {...fieldName.input}
                {...fieldName.meta}
                isError={fieldName.meta.error && fieldName.meta.touched}
                onChange={(event) => {
                    if (props.id === NewCharModalFields.Tags) {
                        // eslint-disable-next-line @typescript-eslint/no-unused-expressions
                        tagsLength(event.target.value) <= 3  ? fieldName.input.onChange(event) : false;
                    } else {
                        fieldName.input.onChange(event);
                    }
                }}
            />
            {fieldName.meta.error && fieldName.meta.touched && <span style={{color: 'red'}}>{fieldName.meta.error}</span>}
        </>
    );
}

export default Input;
