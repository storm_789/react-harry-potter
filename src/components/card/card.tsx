import {FC} from "react";
import {CardContainer, FirstName, LastName, MainInfo} from "./cardStyles/card.styles";
import {ICard} from "./card.type";
import CardInfo from "./cardInfo/cardInfo";
import CharInfoByIdModal from "../../core/containers/pages/characterPage/characterPageModals/charInfoByIdModal/charInfoByIdModal";
import {useAppDispatch, useAppSelector} from "../../store/store.hooks";
import {charactersPageActions} from "../../store/characters/characters.slice";
import {useHistory} from "react-router-dom";

const Card: FC<ICard> = (props) => {
    const { mainInfoStyles, containerStyles, cardInfoProps, character, id} = props;
    const history = useHistory();
    const dispatch = useAppDispatch();
    const isOpen = useAppSelector(state => state.characters.isCharModalByIdOpen);

    const openInfo = () => {
        dispatch(charactersPageActions.toggleCharInfoModalOpen(true))
        if (!isOpen) {
            history.replace(`/characters/${id}`)
        } else {
            history.replace(`/characters`)
        }
    }

    return (
        <CardContainer onClick={openInfo} containerStyles={containerStyles}>
            <MainInfo img={character.img} mainInfoColor={character.mainInfoColor} mainInfoStyles={mainInfoStyles}>
                <FirstName>{character.firstName}</FirstName>
                <LastName>{character.lastName}</LastName>
            </MainInfo>
            <CardInfo {...cardInfoProps}/>
            { isOpen ? <CharInfoByIdModal /> : null }
        </CardContainer>
    );
}

export default Card;
