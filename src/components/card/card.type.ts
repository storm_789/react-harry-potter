import {CSSObject} from "styled-components";

export interface ICardInfo {
    gender: string,
    race: string,
    side: string,
}

export interface ICharacter {
    id: string;
    img: string;
    firstName: string;
    lastName: string;
    cardInfo: ICardInfo;
    cardInfoStyles?: CSSObject;
    cardInfoParamStyles?: CSSObject;
    mainInfoColor: string;
    cardInfoColor: string;
    cardInfoBackGroundColor: string;
    description?: string;
    tags?: string[];
}

export type TCardInfoProps = ICardInfo &
    Pick<ICharacter, 'cardInfoColor' | 'cardInfoBackGroundColor' | 'cardInfoStyles' | 'cardInfoParamStyles'>;

export interface ICard {
    id: string;
    character: Pick<ICharacter, 'img' | 'firstName' | 'lastName' | 'mainInfoColor'>;
    mainInfoStyles?: CSSObject;
    containerStyles?: CSSObject;
    cardInfoProps: TCardInfoProps;
}
