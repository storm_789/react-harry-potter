import styled, {CSSObject} from "styled-components";
import {IMainInfo} from "./cardStyles.type";

export const CardContainer = styled.div<{ containerStyles?: CSSObject }>`
  width: 21vw;
  height: 90%;
  ${props => props.containerStyles}
`;

export const MainInfo = styled.div<IMainInfo>`
  background-image: url(${props => props.img});
  background-size: 100% 100%;
  height: 25%;
  color: ${props => props.mainInfoColor};
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  padding: 25vh 0 2vh .7vw;
  
  ${props => props.mainInfoStyles}
`;

export const FirstName = styled.h3`
  font-weight: 500;
  font-size: 36px;
  line-height: 50px;
  text-shadow: 0 2px 4px rgba(0, 0, 0, 0.08);
`;

export const LastName = styled.h3`
  font-weight: 500;
  font-size: 36px;
  line-height: 50px;
  text-shadow: 0 2px 4px rgba(0, 0, 0, 0.08);
`;
