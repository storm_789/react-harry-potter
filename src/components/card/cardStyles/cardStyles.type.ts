import {CSSObject} from "styled-components";

export interface IMainInfo {
    img: string;
    mainInfoColor: string;
    mainInfoStyles?: CSSObject;
}
