import {FC} from "react";
import {TCardInfoProps} from "../card.type";
import {CardInfoContainer, CharacterParams, Param, ParamContainer} from "./cardInfoStyles/cardInfo.styles";

const CardInfo: FC<TCardInfoProps> = (props) => {
    const { cardInfoColor, cardInfoBackGroundColor, cardInfoStyles, cardInfoParamStyles, ...cardInfo } = props;

    return (
        <CardInfoContainer textColor={cardInfoColor} backGroundTextColor={cardInfoBackGroundColor} cardInfoStyles={cardInfoStyles}>
            <CharacterParams>
                {
                    Object.entries(cardInfo).map((field: [string, string], index: number) =>
                        <ParamContainer key={index} cardInfoParamStyles={cardInfoParamStyles}>
                            <Param>{field[0]}</Param>
                            <Param>{field[1]}</Param>
                        </ParamContainer>
                    )
                }
            </CharacterParams>
        </CardInfoContainer>
    );
};

export default CardInfo;
