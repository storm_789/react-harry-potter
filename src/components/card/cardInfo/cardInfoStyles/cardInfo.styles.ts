import styled, {CSSObject} from "styled-components";

export const CardInfoContainer = styled.div<{textColor: string, backGroundTextColor: string, cardInfoStyles?: CSSObject}>`
  padding: 17px;
  color: ${props => props.textColor};
  background-color: ${props => props.backGroundTextColor};
  
  ${props => props.cardInfoStyles}
`;

export const CharacterParams = styled.ul`
  display: flex;
  flex-direction: column;
  li:last-child {
    border: none;
  }
`;

export const Param = styled.p`
  font-weight: normal;
`;

export const ParamContainer = styled.li<{cardInfoParamStyles?: CSSObject}>`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid #F1F1F1;
  line-height: 40px;
  
  ${props => props.cardInfoParamStyles}
`;
