import styled from "styled-components";

export const PaginationContainer = styled.div`
  display: flex;
  gap: 15px;
  justify-content: center;
  &>button {
    background: none;
    padding: 0;
    border: none;
    cursor: pointer;
  }
`;

export const PagesButtonContainer = styled.div`
  display: flex;
  gap: 15px;
  align-items: center;
  width: 120px;
  overflow: hidden;
`;

export const PageButton = styled.button<{currentPage: number, buttonId: number}>`
  display: flex;
  width: 10px;
  height: 10px;
  border-radius: 50%;
  background-color: ${props => props.currentPage === props.buttonId ? '#B09A81' : 'white'};
  cursor: pointer;
  border: none;
`;
