import React, {FC, useEffect, useState} from "react";
import {IPagination} from "./pagination.type";
import {PageButton, PagesButtonContainer, PaginationContainer} from "./pagination.styles";
import {onFocus} from "@reduxjs/toolkit/dist/query/core/setupListeners";


const Pagination: FC<IPagination> = (props) => {
    const { paginationList, currentPage, setCurrentPage, prevButtonImg, nextButtonImg } = props;

    const [buttonList, setButtonList] = useState<JSX.Element[]>([]);
    const [numVisibleButton, setNumVisibleButton] = useState<number>(0);

    const changeButtonList = () => {
        setButtonList([]);
        paginationList.map((circle, idx) => {
            setButtonList(prev => [...prev,
                <PageButton
                    key={idx}
                    currentPage={currentPage}
                    buttonId={circle.id}
                    id={idx.toString()}
                    onClick={(event) => {
                        setCurrentPage(circle.id);
                    }}
                >{null}</PageButton>])
        })
    }

    useEffect(() => {
        if (currentPage === numVisibleButton + 2) {
            setNumVisibleButton(prev => prev + 1);
        } else if (currentPage === numVisibleButton && numVisibleButton !== 0) {
            setNumVisibleButton(prev => prev - 1);
        }
    }, [currentPage])

    useEffect(() => {
        changeButtonList();
    }, [paginationList]);

    useEffect(() => {
        changeButtonList();
    }, [currentPage])

    return (
        <div style={{width: '100%'}}>
            <PaginationContainer>
                <button onClick={() => paginationList[currentPage - 1] ? setCurrentPage(prev => prev - 1) : false}>
                    <img src={prevButtonImg} alt=""/>
                </button>
                <PagesButtonContainer>
                    <div style={{ display: 'flex', gap: '45px'}}>
                        {buttonList.slice(numVisibleButton, numVisibleButton + 3).map((button, idx) => button)}
                    </div>
                </PagesButtonContainer>
                <button onClick={() => paginationList[currentPage + 1] ? setCurrentPage(prev => prev + 1) : false}>
                    <img src={nextButtonImg} alt=""/>
                </button>
            </PaginationContainer>
        </div>
    )
}

export default Pagination;
