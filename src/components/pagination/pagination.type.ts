import {Dispatch, SetStateAction} from "react";
import {IPaginationList} from "../registry/registry.type";

export interface IPagination {
    paginationList: IPaginationList[];
    currentPage: number;
    setCurrentPage: Dispatch<SetStateAction<number>>;
    prevButtonImg: string;
    nextButtonImg: string;
}
