import {IButton} from "./button.type";
import React, {FC} from "react";
import {StyledButton} from "./buttonStyles/button.styles";
import {useHistory} from "react-router-dom";

const StartButton: FC<IButton> = (props) => {
    const { children, mainColor, pressedColor, hoverColor, buttonStyles, path } = props;
    const history = useHistory();

    const handleButtonClick = (event: React.MouseEvent): void => {
        history.replace(path);
    }

    return (
        <StyledButton
            mainColor={mainColor}
            hoverColor={hoverColor}
            pressedColor={pressedColor}
            buttonStyles={buttonStyles}
            onClick={handleButtonClick}
        >
            {children}
        </StyledButton>
    );
};

export default StartButton;
