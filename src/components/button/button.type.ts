import {CSSObject} from "styled-components";

export interface IButton {
    mainColor: string;
    hoverColor: string;
    pressedColor: string;
    buttonStyles?: CSSObject;
    path: string;
}
