import {IButton} from "../button.type";

export type TButtonStylesTypes = Omit<IButton, 'path'>;

