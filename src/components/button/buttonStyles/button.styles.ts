import styled from 'styled-components';
import {TButtonStylesTypes} from "./buttonStyles.type";


export const StyledButton = styled.button<TButtonStylesTypes>`
  width: 180px;
  height: 60px;
  background: ${ props => props.mainColor };
  border-radius: 8px;
  font-size: 20px;
  line-height: 26px;
  cursor: pointer;
  border: none;

  ${props => props.buttonStyles}
  
  &:hover {
    background: ${ props => props.hoverColor };
  }
  
  &:active {
    background: ${ props => props.pressedColor };
  }
`
