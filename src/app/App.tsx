import React from 'react';

import { RouteSwitcher } from "../core/components/router/router";
import Header from "../components/header/header";
import {BrowserRouter} from "react-router-dom";
import '../styles/customCheckBox.css';
import logo from '../styles/img/logo.png';
import {Provider} from "react-redux";
import {store} from "../store";

function App() {

    const navbarItems = {
        '/': 'Главная',
        characters: 'Персонажи'
    }

    return (
        <Provider store={store}>
            <BrowserRouter>
                <Header headerProps={{headerLogo: logo}} navbarProps={{items: navbarItems}}/>
                <RouteSwitcher />
            </BrowserRouter>
        </Provider>
    );
}

export default App;
